$(
    function ()
    {


        checkWeather();


        $("#goBtn").click(function () {

            checkWeather()

            }
        );

        $("#goBtn").change(function () {

            checkWeather()

            }
        );



    }
);


function checkWeather()
{
    $.ajax(
        {
            url: "http://api.openweathermap.org/data/2.5/weather?q="+$("#search").val()+"&appid=a8ca897e361c4fb52d6cf07566e5a3f4&units=metric",
            success: function (data) {

                // console.log("Success");
                // console.log(data);
                $("#locationText").text("Weather in "+ data.name);
                $("#mainWeather").html(Math.ceil(data.main.temp) + "<sup><span>o</span> &nbsp; c</sup>");
                $(".weatherIcon").attr("src", "images/"+data.weather[0].icon+".png");
                $(".weatherDesc").html(data.weather[0].description);
            },
            dataType: "json",
            type: "GET",
            headers: true
        }
    );

}

function init()
{
    console.log("page init");
}


